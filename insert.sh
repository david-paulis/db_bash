#!/usr/bin/bash
#  $1 -> database $2 -> tablename
#function to keep calling type_check string incase of wrong data type

#check primary key
function check_pk {
	if [ -z "$value" ]; then
		null="true"
		echo "Primary Key Can't Be NULL"
	else
		for col in $arr
		do
			if test "$value" == "$col"
			then
				null="false"
				pkExist="true"
				break
			else
				null="false"
				pkExist="false"
			fi
	done
	fi
}

function get_pk {
	echo "Primary Key Exist Enter Valid Primary Key or Q for exit"
	read -r value
	if [ $value = "q" ] || [ $value = "Q" ]
	then 
		back=1
		break
	else
		check_pk
	fi
}

function check
{
	local type=$1
	echo Invalid data type please enter $type or Q for exit
	if [ $type = "str" ]
	then
		echo "[Please note that string not allowed to have | ]"
	fi
	read
	./type_check.sh $type $REPLY
	local state=$?
	if [ $REPLY = "q" ] || [ $REPLY = "Q" ]
	then 
		back=1
	else
		while [ $state -ne 0 ]
		do
			echo Invalid data type please renter $type or Q for exit
			read
			if [ $REPLY = "q" ] || [ $REPLY = "Q" ]
			then 
				back=1
				break
			else
				./type_check.sh $type $REPLY 
			fi
		done
		value=$REPLY 
		check_pk
		if test $pkExist == "false"
		then
			if [ -z $row ]
			then
				row+=$REPLY
			else
				row+="|"$REPLY
			fi
		else
			get_pk
			
		fi
	fi
}

touch .temp
row=""
#get table schema from schema file and echo to temp
dir="~/DatabaseEngine"
x=`cat $dir/$1/.schema|grep -w $2`
echo $x > .temp
typeset -i counter=`wc -w .temp|cut -f1 -d" "`
counter=$counter-1 #contain number of columns
typeset -i y=2
pk=`cut -f2 -d" " .temp|cut -f1 -d:`
pkExist="false"
arr=`cut -f1 -d"|" $dir/$1/$2` #all pks
typeset -i back=0
#take each column data from user
while [ $counter -gt 0 ] && [ $back -eq 0 ]
do 
	column=`cut -f$y -d" " .temp|cut -f1 -d:` #column name
	type=`cut -f$y -d" " .temp|cut -f2 -d:` #column type
	counter=$counter-1
	y=$y+1
	echo Enter $column value
	read -r value
	#check for pk
    if test $y -eq 3
    then
    	check_pk
    	null="true"
    	while [ $pkExist == "true" ]
    	do
			get_pk
			
        done
		if [ $back -eq 0 ]
		then
			#./type_check.sh $type $value
			./type_check.sh $type "$value"
			#check for script exit status to validate type
			if [ $? -eq 0 ]
			then
				check_pk
				if test $pkExist == "false"
				then
					row+=$value
				else
					echo Invalid Primary Key Back to Open DB menu
				fi
			else
				check $type
			fi
		fi
	else
		./type_check.sh $type "$value"
		if [ $? -eq 1 ]
		then
			check $type
		else
		#check for script exit status to validate type
			if [ $back -eq 0 ]
			then
				if [ $? -eq 0 ]
				then
					if [ -z $row ]
					then
						row+=$value
					else
						row+="|"$value
					fi
				else
					check $type
				fi
			fi
		fi
	fi
done
if [ $back -eq 0 ]
then
	echo $row >> $dir/$1/$2
fi
rm .temp