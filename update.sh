#!/usr/bin/bash 
# $1 database ,$2 table
dir="~/DatabaseEngine"
data=$dir/$1/$2
db=$dir/$1
table=$2

function getRowNum {
    rowNum=$(awk -v pk=$1 'BEGIN{ FS="|";}{if($1==pk){print NR;}}' $data)
    row=$(awk -v pk=$1 'BEGIN{ FS="|";}{if($1==pk){print $0;}}' $data)
}

function isEmpty {
typeset -i num=$(cat $data | wc -l)
if [[ $num -eq 0 ]]
then
   return 0;
else
  return 1;
fi
}

function isColumnExist {
  # $1 database ,$2 table,$3 column
  rowSchema=`awk '$1 ~ /^'$table'$/' $db/.schema | grep -c $3 `
}

function updateColumn {
  # $1 table path , $2 value to update , $3 new value
  #sed -i 's/original/new/g' file.txt
  typeset str=$rowNum"s/$2/$3/g"
  sed -i "$str" "$1"
}

typeset -i counter=0
function getIndex {
   # $1 table name , $2 col name
   rowSc=$(awk -v pk=$1 'BEGIN{ FS=" ";}{if($1==pk){print $0;}}' "$db/.schema" ) 
   arr=$(echo $rowSc | tr " " "\n")
   for part in $arr
   do
   pattern="^$2[:]*"
   if [[ $part =~ $pattern ]]
    then 
     return ;
   fi 
   counter=$counter+1
   done
}
function getOldValue {
  oldvalue=$(echo $row | cut -d"|" -f$counter)
}
isEmpty
if [[ $? -eq 1 ]]
then
  flag="true";
  while test $flag == "true" 
  do
    ./view.sh $db $table
    echo enter your pk
    read 
    getRowNum $REPLY
    if [[ -z $rowNum ]]
    then  
      echo no pk with $REPLY exist in $2
    else
    # sed -i $rowNum'd' $data
      flag=="false";
    fi
  done
  echo "1) Update all column"
	echo "2) update column"
	read -p "-> " -r input
  case $input in
  1)  echo enter new row values
    sed -i $rowNum'd' $data
    ./insert.sh $1 $2
  ;;
  2)echo enter column name and new value seprated by space
  read colName newVal
  isColumnExist $1 $2 $colName
  if (( $rowSchema == 1 ))
  then
    getIndex $2 $colName
    if [[ $counter -eq 1 ]]
    then
      echo primary key must not be updates 
    else
      getOldValue
      updateColumn $data $oldvalue $newVal
    fi
  else
    echo $colName not exist in table $2
  fi
  ;;
  *)echo "Wrong input"
  esac
  echo row updated
else 
   echo table $2 is isEmpty
fi
