#!/usr/bin/bash 
# $1 database ,$2 table
#missing show table first
dir="~/DatabaseEngine"
data=$dir/$1/$2
function getRowNum {
  rowNum=$(awk -v pk=$1 'BEGIN{ FS="|";}{if($1==pk){print NR;}}' $data)
  row=$(awk -v pk=$1 'BEGIN{ FS="|";}{if($1==pk){print $0;}}' $data)
}
./view.sh $dir/$1 $2
flag="true";
typeset -i back=0
while [ $flag == "true" ] && [ $back -eq 0 ]
do
  echo Enter the primary key of the row you want to delete or Q to exit
  read -p "-> " -r replay
  if [ $replay = "q" ] || [ $replay = "Q" ]
  then
    back=1
  else
    getRowNum $replay
    if [[ -z $rowNum ]]
      then
      echo No row with $REPLY primary key exist in $2
      
    else
      sed -i $rowNum'd' $data
      flag="false"
    fi
  fi
done
if [ $back -eq 0 ]
then 
 echo row deleted
fi
  
