#!/usr/bin/bash
function isNumber {
	local validNum="^[0-9]*$"
	if [ $1 =   2> /dev/null ]
	then
		#echo "not a number"
		return 0
	elif [[ $1 =~ $validNum ]]
	then
		#echo "a number"
		return 1
	else
		#echo "not a number"
		return 0
	fi
}
isNumber "$1"
