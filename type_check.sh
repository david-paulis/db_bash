#!/usr/bin/bash

case $1 in
int)
	allowed="^[0-9]*$" 
	if [[ ! $2 =~ $allowed ]]
	then
		exit 1
	else 
		exit 0
	fi
;;
str)
allowed="^[a-zA-Z][0-9|a-z|A-Z|_|-|/|@|#|\| |!|%|(|)|+|~]*$" 
if [[ ! $2 =~ $allowed ]] 
then
	#echo "failed test 1"
	exit 1
elif [ $2 =   2> /dev/null ] 
then
	#echo "failed test 2"
	exit 1
elif [ ! `grep -o $2 <<<"$2 " | wc -c ` -gt 0 ] || [ ! `grep -o $2  <<<"$2 " | wc -c `  -lt 30 ]
then
	#echo "failed test 3"
	exit 1
else
	exit 0
fi
;;
esac
