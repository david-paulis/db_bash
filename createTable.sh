#!/usr/bin/bash
# $1 database
export LC_COLLATE=C
shopt -s extglob
dbPath="~/DatabaseEngine/$1"
tableName=$1

function isValidType {
typeset arr[2]=["int","str"];
for allowedType in "int" "str"
do
	if [[ $1 == $allowedType ]]
	then
		return 0;
	fi
done
return 1;
}

function isValidName {
# $1 name
local valid="^[a-zA-Z][0-9|a-z|A-Z|_]*$"
if [[ ! $1 =~ $valid ]]
then 
	return 1;
else 
	return 0;
fi
}
# take table name and columns
echo Enter table name
read -r tableName
./isValidName.sh $dbPath "$tableName"
typeset -i valid=$?
if [ $valid -eq 1 ]
then
	echo "Enter how many columns in the table"
    read -r column
	./isNum.sh "$column"
	typeset -i isNum=$?
	if [ $isNum -eq 1 ]
	then
      	schema=$tableName
		typeset -i save=0
		typeset -i counter=1;
		typeset -i flag=0
		while [ $counter  -le  $column ] && [ $flag -eq 0 ]
		do 
			
		  		if [ $counter -eq 1 ]
		   		then 
		     			echo Enter primary key
						read -r columnName
					if isValidName "$columnName"
					then
						echo "Enter primary key's type [Allowed types are int for integer and str for string ] "
						read -r type
						flag=1
					else 
						echo Error wrong input
						flag=1
					fi
		   		else 
		     		echo Enter column $counter name
					read -r columnName
					if isValidName "$columnName"
					then
						echo " Enter column $counter 's type [Allowed types are int for integer and str for string ] "
						read -r type
						flag=1
					else 
						echo Error wrong input
						flag=1
					fi
		   		fi
		 		if isValidName $columnName  && isValidType $type
		   		then 
		     			flag=0
						counter=$counter+1
						typeset -i save=1
		     			schema+=" $columnName:$type"
		  		fi 
		done
		if [ $save -eq 1 ]
		then
		 	# save schema and table data
			echo $schema >> $dbPath/.schema
			touch $dbPath/$tableName
		fi
	else
		echo "column numbers Make sure it's a number and at leaset 2"
	fi
     	#echo Wrong Name[table name should be unique start with charcter and doesn\'t have special charcters except _ ]
fi

