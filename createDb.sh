#!/usr/bin/bash
dbName="$1"
dir="~/DatabaseEngine"
./isValidName.sh $dir "$dbName"
typeset -i valid=$?
if [ $valid -eq 1 ]
then
	mkdir $dir/$dbName
fi