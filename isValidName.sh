#!/usr/bin/bash
#$2 is the name of the database or the table
#$1 is the dir
function isValidName {
	local name="$2"
	local dir=$1
	local pattern="^[a-zA-Z][0-9|a-z|A-Z|_]*$"
	#cNum=`grep -o $dbName  <<<"$dbName " | wc -c ` 
	if [ $name =   2> /dev/null ]
	then
		echo "Please Make sure to enter a valid name"
		return 0
	elif [[ ! $name =~ $pattern ]]
	then
		echo "Please write a valid name"
		return 0
	elif [ ! `grep -o $name <<<"$name " | wc -c ` -gt 0 ] || [ ! `grep -o $name  <<<"$name " | wc -c `  -lt 25 ]
	then
		echo "Sorry the name can't exceed 25 charcter"
		return 0
	elif [ `find $dir -name $name 2> /dev/null`  = $dir/$name 2> /dev/null ]
	then
		echo "Error it's Already exist"
		return 0
	else
		return 1
	fi

}
isValidName $1 "$2"
