#!/usr/bin/bash 
dir="~/DatabaseEngine"
dbFolder="DatabaseEngine"
typeset -i flag=0

function exitMsg {
	echo "			Thanks for using our database engine"
}

function clearScreen {
	sleep 1
	clear
}

clear
while [ $flag -ne 1 ]
do
	echo "1) Create DataBase"
	echo "2) Manipulate DataBase"
	echo "3) Delete DataBase"
	echo "4) Exit" 
	read -p "-> " -r input
	./isNum.sh "$input"
	typeset -i num=$?
	if [ $num -eq 1 ]
	then
		typeset -i input=$input
		clearScreen
		case $input in
			1)echo "Please enter the DataBase name"
			read -r dbName
			./createDb.sh "$dbName"
			;;
			2)echo Choose the database you want to manipulate
			. ./showData.sh $dbFolder 1
			if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
			then
				clearScreen
				db=$choosedName
				typeset -i flag2=0
				while [ $flag2 -ne 1 ]
				do
					echo "1) Create Table"
					echo "2) Manipulate Table"
					echo "3) Delete Table"
					echo "4) Show Tables"
					echo "5) Exit" 
					read -p "-> " -r input2
					clearScreen
					case $input2 in
						1)./createTable.sh $db
						;;
						2)typeset -i flag3=0
						while [ $flag3 -ne 1 ]
						do
							echo "1) Add row"
							echo "2) Update row"
							echo "3) Delete row"
							echo "4) Show rows"
							echo "5) Exit" 
							read -p "-> " -r input3
							case $input3 in
								1)clearScreen
								echo Choose your table
								. ./showData.sh $dbFolder/$db 1
								if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
								then
									./insert.sh $db $choosedName
								fi
								;;
								2)clearScreen
								echo Choose your table
								. ./showData.sh $dbFolder/$db 1
								if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
								then
									./update.sh $db $choosedName
								fi
								;;
								3)clearScreen
								echo Choose your table
								. ./showData.sh $dbFolder/$db 1
								if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
								then
									./deleteRow.sh $db $choosedName
								fi
								;;
								4)clearScreen
								echo Choose your table
								. ./showData.sh $dbFolder/$db 1
								if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
								then
									./view.sh $dir $choosedName
								fi
								;;
								5)flag3=1
								;;
							esac
						clearScreen
						done
						;;
						3). ./showData.sh $dbFolder/$db 1
						if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
						then
							./delete.sh $db $choosedName table
						fi
						;;
						4)./showData.sh $dbFolder/$db 0
						;;
						5)flag2=1
						;;
					esac
				clearScreen
				done
			fi
			;;
			3). ./showData.sh $dbFolder 1
			if [ $returnState -eq 0 ] && [ $empty -ne 1 ]
			then
				./delete.sh " " $choosedName database
			fi
			flag=0
			;;
			4)flag=1
			exitMsg
			;;
		esac
	else
		echo "Wrong input"
	fi
	clearScreen
done
