#!/usr/bin/bash
#return state represent if it successed or not
dir="~/"
dataSource=$1
choose=$2
empty=0
#$1 name of the place to show its data
function getData {
	typeset -i i=0
	typeset -A dbs
	dir="$dir$1"
	local db
	for db in `ls $dir`
	do
		dbs[$i]=$db
		i=$i+1
	done
	if [ $i -ne 0 ]
	then
		showData ${dbs[*]}
	else
		echo "It's Empty"
		empty=1
	fi
}
function showData {
	typeset -i i=1
	local input
	returnState=1
	data=$*
	for index in $data
	do
	    echo "$i - $index"
		i=$i+1
	done
	if [ $i -gt 1 ] && [ $choose -eq 1 ]
	then
		read -p "Enter your choice -> " -r input
		#echo "$input"
		if [ $input =   2> /dev/null ]
		then
			echo "Wrong input"
		else 
			./isNum.sh "$input"
			typeset -i num=$?
			if [ $num -eq 1 ] && [ $input -gt 0 ] && [ $input -lt $i ]
			then 
				#echo "$input"
				typeset -i input=$input-1
				#echo "$input"
				typeset -i j=0
				for d in $data
				do
						if [ $j -eq $input ]
					then
						cName=$d
						#echo choosing name = $cName
					fi
					j=$j+1
				done
				returnState=0
			else
				echo "Wrong input"
			fi
		fi
	fi

}
getData $dataSource
choosedName=$cName
