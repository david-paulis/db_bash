#!/usr/bin/bash 
#$1 is the main folder if it's a table
#$2 is the deleted name folder or table
#$3 is the type database or table
dir="~/DatabaseEngine"
choosed=$2
dtype=$3
echo Are you sure you want to delete $choosed $dtype [Y]
read confirm
if [ $confirm = "y" ] || [ $confirm = "Y" ]
then 
	if [ $dtype = "database" ]
	then
		rm -r $dir/$choosed
	else
		rm $dir/$1/$choosed
	fi
	echo $choosed Has been deleted
fi
