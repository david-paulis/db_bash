#!/usr/bin/bash
# $1 db ,$2 tablename
db="$1"
function extractInfo {
    # $1 table_name
  #table_name =$(echo $schema_row | cut -f1 -d" ")
  #arr=$(echo $schema_row | tr " " "\n")
  #sc=$(echo $schema_row | xargs -r sed -nie 's/int//' )
  schema_row=$(awk -v pk=$1 'BEGIN{ FS=" ";}{if($1==pk){print $0;}}' "$db/.schema" ) 
  table=$(echo $schema_row | cut -f1 -d" " )
  columns=$(echo $schema_row | cut -f2- -d" " )
}
if [ $# -lt 2 ]
then
  echo "must pass db , tablename to $0"
else 
  extractInfo $2
  echo "			*************$table**************"
  typeset -i numOfLines=$(cat $db/$2 | wc -l);
  if [[ numOfLines -eq 0 ]]
  then
    echo Table $table is empty
  else
    echo $columns 
    more  $db/$2 | tr "|" "\t"
  fi
fi